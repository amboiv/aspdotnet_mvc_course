﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Udemy_ASPDotNET_MVC_Course.Startup))]
namespace Udemy_ASPDotNET_MVC_Course
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
