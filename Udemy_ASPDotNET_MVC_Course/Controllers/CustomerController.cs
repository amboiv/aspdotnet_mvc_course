﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using Udemy_ASPDotNET_MVC_Course.Models;
using Udemy_ASPDotNET_MVC_Course.ViewModels;

namespace Udemy_ASPDotNET_MVC_Course.Controllers
{
	public class CustomerController : Controller
	{
		private ApplicationDbContext _context;

		public CustomerController ()
		{
			_context = new ApplicationDbContext ();
		}

		protected override void Dispose (bool disposing)
		{
			_context.Dispose ();
		}

		public ActionResult New ()
		{
			var membershipTypes = _context.MembershipTypes.ToList ();
			var viewModel = new CustomerFormViewModel
			{
				Customer = new Customer (),
				MembershipTypes = membershipTypes
			};
			return View ("CustomerForm",viewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize (Roles = RoleName.CanManageMovies)]
		public ActionResult Save (Customer customer)
		{
			if (!ModelState.IsValid)
			{
				var viewModel = new CustomerFormViewModel
				{
					Customer = customer,
					MembershipTypes = _context.MembershipTypes.ToList ()
				};
				return View ("CustomerForm", viewModel);
			}

			if (customer.Id == 0)
			{
				_context.Customers.Add (customer);
			}
			else
			{
				var customerInDb = _context.Customers.Single (c => c.Id == customer.Id);

				customerInDb.Name = customer.Name;
				customerInDb.Birthdate = customer.Birthdate;
				customerInDb.MembershipTypeId = customer.MembershipTypeId;
				customerInDb.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
			}
			_context.SaveChanges ();

			return RedirectToAction ("Index", "Customer");
		}

		// GET: Customer
		public ViewResult Index()
		{
			// TODO: Only an example on how to cache data from db
			//if (MemoryCache.Default["Genres"] == null)
			//{
			//	MemoryCache.Default["Genres"] = _context.Genres.ToList ();
			//}
			//var genres = (IEnumerable <Genre>)MemoryCache.Default["Genres"];

			if (User.IsInRole (RoleName.CanManageMovies))
			{
				return View ("Index");
			}

			return View ("ReadOnlyIndex");
		}

		[Authorize (Roles = RoleName.CanManageMovies)]
		public ActionResult Details (int id)
		{
			var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault (c => c.Id == id);

			if (customer == null)
			{
				return HttpNotFound ();
			}
			return View (customer);
		}



		/*private IEnumerable<Customer> GetCustomers ()
		{
			return new List<Customer>
			{
				new Customer { Id = 1, Name = "John Smith" },
				new Customer { Id = 2, Name = "Mary Williams" }
			};
		}*/

		[Authorize (Roles = RoleName.CanManageMovies)]
		public ActionResult Edit (int id)
		{
			var customer = _context.Customers.SingleOrDefault (c => c.Id == id);

			if (customer == null)
			{
				return HttpNotFound();
			}

			var viewModel = new CustomerFormViewModel
			{
				Customer = customer,
				MembershipTypes = _context.MembershipTypes.ToList ()
			};
			return View ("CustomerForm", viewModel);
		}
	}
}