﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Udemy_ASPDotNET_MVC_Course.Dtos;
using Udemy_ASPDotNET_MVC_Course.Models;

namespace Udemy_ASPDotNET_MVC_Course.Controllers.Api
{
	public class NewRentalController : ApiController
	{
		private ApplicationDbContext _context;

		public NewRentalController ()
		{
			_context = new ApplicationDbContext ();
		}

		[HttpPost]
		public IHttpActionResult CreateNewRentals (NewRentalDto newRental)
		{
			//if (newRental.MovieIds.Count == 0)
			//{
			//	return BadRequest ("No movie Ids have been given.");
			//}

			var customer = _context.Customers.Single (c => c.Id == newRental.CustomerId);

			//var customer = _context.Customers.SingleOrDefault (c => c.Id == newRental.CustomerId);
			
			//if (customer == null)
			//{
			//	return BadRequest("CustomerId is not valid.");
			//}

			var movies = _context.Movies.Where (m => newRental.MovieIds.Contains (m.Id)).ToList ();

			//if (movies.Count != newRental.MovieIds.Count)
			//{
			//	return BadRequest("One or more MovieIds are invalid.");
			//}

			foreach (var movie in movies)
			{
				if (movie.NumberAvailable == 0)
				{
					return BadRequest ("Movie is not available. " + movie.Name);
				}	
			
				movie.NumberAvailable--;

				var rental = new Rental
				{
					Customer = customer,
					Movie = movie,
					DateRented = DateTime.Now
				};
				_context.Rentals.Add (rental);
			}

			_context.SaveChanges ();

			return Ok();
		}
	}
}
