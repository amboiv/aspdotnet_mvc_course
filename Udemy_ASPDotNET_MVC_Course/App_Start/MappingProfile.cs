﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Udemy_ASPDotNET_MVC_Course.Dtos;
using Udemy_ASPDotNET_MVC_Course.Models;

namespace Udemy_ASPDotNET_MVC_Course.App_Start
{
	public class MappingProfile : Profile
	{
		public MappingProfile ()
		{
			Mapper.CreateMap <Customer, CustomerDto> ();
			Mapper.CreateMap <CustomerDto, Customer> ().ForMember (c => c.Id, opt => opt.Ignore ());

			Mapper.CreateMap <Movie, MovieDto> ();
			Mapper.CreateMap<MovieDto, Movie> ().ForMember (m => m.Id, opt => opt.Ignore ());

			Mapper.CreateMap <MembershipType, MembershipTypeDto> ();
			//Mapper.CreateMap <MembershipTypeDto, MembershipType> ().ForMember (c => c.Id, opt => opt.Ignore ());

			Mapper.CreateMap<Genre, GenreDto> ();
		}
	}
}