﻿using System.Web;
using System.Web.Mvc;

namespace Udemy_ASPDotNET_MVC_Course
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters (GlobalFilterCollection filters)
		{
			filters.Add (new HandleErrorAttribute ());
			filters.Add (new AuthorizeAttribute ());	// To globally check if user is authorized/logged in
			filters.Add (new RequireHttpsAttribute());	// To force access to site through https protocol
		}
	}
}
