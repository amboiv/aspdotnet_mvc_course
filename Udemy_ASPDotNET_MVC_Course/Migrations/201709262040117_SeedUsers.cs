namespace Udemy_ASPDotNET_MVC_Course.Migrations
{
	using System;
	using System.Data.Entity.Migrations;
	
	public partial class SeedUsers : DbMigration
	{
		public override void Up()
		{
			Sql (@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1f90a7d8-1fec-4377-a9c3-56847817c6eb', N'guest@rentalot.com', 0, N'AEu+STN7f1aqx1CPaCDReXA+YD+TmXSI8ELXNbORjBExTpxGMMhjCHtY70hk2s2VSA==', N'd38d644a-c1d5-4690-9e09-e98fdd689643', NULL, 0, 0, NULL, 1, 0, N'guest@rentalot.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8a2ca73e-ca46-450a-8946-79171e71930d', N'admin@rentalot.com', 0, N'ALqfMGp06yJP0Qp1Wvf8rR3sQ3ZGw4BkbfCZhRuZT8A7FNDLBHHFmPM1foxA0McwxA==', N'cb8b4fa0-7792-4ce2-a48b-0036381b74a5', NULL, 0, 0, NULL, 1, 0, N'admin@rentalot.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'596c72e0-f41b-458d-a0a3-bb05a3cbc958', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8a2ca73e-ca46-450a-8946-79171e71930d', N'596c72e0-f41b-458d-a0a3-bb05a3cbc958')
");
		}
		
		public override void Down()
		{
		}
	}
}
