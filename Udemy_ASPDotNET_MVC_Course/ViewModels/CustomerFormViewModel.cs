﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Udemy_ASPDotNET_MVC_Course.Models;

namespace Udemy_ASPDotNET_MVC_Course.ViewModels
{
	public class CustomerFormViewModel
	{
		public IEnumerable <MembershipType> MembershipTypes { get; set; }
		public Customer Customer { get; set; }
	}
}