﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Udemy_ASPDotNET_MVC_Course.Models;

namespace Udemy_ASPDotNET_MVC_Course.ViewModels
{
	public class RandomMovieViewModel
	{
		public Movie Movie { get; set; }
		public List <Customer> Customers { get; set; }
	}
}