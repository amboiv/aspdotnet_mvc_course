﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Udemy_ASPDotNET_MVC_Course.Models;

namespace Udemy_ASPDotNET_MVC_Course.Dtos
{
	public class CustomerDto
	{
		public int Id { get; set; }

		[Required]
		[StringLength (255)]
		public string Name { get; set; }

		public bool IsSubscribedToNewsletter { get; set; }

		[Required]
		public byte MembershipTypeId { get; set; }

		public MembershipTypeDto MembershipType { get; set; }

		//[Min18YearsIfAMember] TODO: Fix conflicting create Methods In customerController and Api/CustomerController
		public DateTime? Birthdate { get; set; }
	}
}