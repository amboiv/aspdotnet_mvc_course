﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Udemy_ASPDotNET_MVC_Course.Models
{
	public class Customer
	{
		public int Id { get; set; }

		[Required]
		[StringLength(255)]
		public string Name { get; set; }

		public bool IsSubscribedToNewsletter { get; set; }

		public MembershipType MembershipType { get; set; }

		[Display(Name = "Membership Type")]
		public byte MembershipTypeId { get; set; }  // EntityFramework recognices this convention, and treats this prop as a foreign key
													// Type is byte which makes it implicitly [Required]
		[Min18YearsIfAMember]
		[Display(Name = "Date of Birth")]
		public DateTime? Birthdate { get; set; }
	}
}