﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Razor;
using System.Web.UI.WebControls;

namespace Udemy_ASPDotNET_MVC_Course.Models
{
	public class Movie
	{
		public int Id { get; set; }

		[Required]
		[StringLength(255)]
		public string Name { get; set; }

		public Genre Genre { get; set; }

		[Display(Name = "Genre")]
		public byte GenreId { get; set; }

		public DateTime DateAdded { get; set; }

		[Required]
		[Display (Name = "Release Date")]
		public DateTime ReleaseDate { get; set; }

		[Range(1, 25)]
		[Display (Name = "Number in Stock")]
		public byte NumberInStock { get; set; }

		[Display (Name = "Number Available")]
		public byte NumberAvailable { get; set; }
	}
}